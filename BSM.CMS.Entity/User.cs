﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class User:BaseEntity
    {
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OrganizationName { get; set; }
        public string PhoneNo { get; set; }
        public string OrganizationsAddress { get; set; }
        public string City { get; set; }
        public long Pincode { get; set; }

    }
}
