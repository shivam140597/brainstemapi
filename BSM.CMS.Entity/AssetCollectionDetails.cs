﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class AssetCollectionDetails
    {
        [Key]
        public int AssetCollectionDetailsId { get; set; }
        
        
        public bool ActiveInd { get; set; }

        [ForeignKey("AssetCollectionMaster")]
        public int AssetCollectionMasterid { get; set; }
        public AssetCollectionMaster AssetCollectionMaster { get; set; }
        [ForeignKey("assetMaster")]
        public int AssetmasterId { get; set; }
        public AssetMaster assetMaster { get; set; }    


    }
}
