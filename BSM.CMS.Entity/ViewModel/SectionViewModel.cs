﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class SectionViewModel
    {
        
        public int SectionMasterId { get; set; }
        
        public string SectionCode { get; set; }
        
        public string SectionName { get; set; }
        public int CompId { get; set; }
    }
}
