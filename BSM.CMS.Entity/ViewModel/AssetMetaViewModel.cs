﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class AssetMetaViewModel
    {
        public int AssetMetaId { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
        public bool ActiveInd { get; set; }

        //[ForeignKey("Asset")]
        public int AssteMasterId { get; set; }
    }
}
