﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class CollectionViewModel:BaseViewModel
    {
        public int AssetCollectionMasterId { get; set; }
        public string CollectionName { get; set; }
        public string Description { get; set; }
        public int CompId { get; set; }
    }
}
