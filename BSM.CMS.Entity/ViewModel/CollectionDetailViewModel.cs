﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class CollectionDetailViewModel:BaseViewModel
    {
        public int AssetCollectionDetailsId { get; set; }
        public bool ActiveInd { get; set; }
        public int AssetCollectionMasterid { get; set; }
        public int AssetmasterId { get; set; }
    }
}
