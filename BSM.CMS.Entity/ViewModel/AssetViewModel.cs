﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class AssetViewModel:BaseViewModel
    {
        public int AssetmasterId { get; set; }
        public string AssetName { get; set; }
        public string Description { get; set; }
        public int AssetType { get; set; }
        public string Assetpath { get; set; }
        public int status { get; set; }
        public int CompId { get; set; }
        public int SectionMasterId { get; set; }
    }
}
