﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity.ViewModel
{
    public class BaseViewModel
    {
        public int StatusCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
