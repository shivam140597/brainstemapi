﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class AssetCollectionMaster:BaseEntity
    {
        [Key]
        public int AssetCollectionMasterId { get; set; }
        public string CollectionName { get; set; }
        public string Description { get; set; }
        public int CompId { get; set; }
    }
}
