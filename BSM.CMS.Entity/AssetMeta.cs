﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class AssetMeta
    {
        [Key]
        public int AssetMetaId { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
        public bool ActiveInd { get; set; }

        [ForeignKey("Asset")]
        public int AssteMasterId { get; set; }
        public AssetMaster Asset { get; set; }

    }
}
