﻿using System;

namespace BSM.CMS.Entity
{
    public class BaseEntity
    {
        public int CreatedBy { get; set; } = 0;
        public DateTime CreatedDttm { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
        public DateTime? UpdatedDttm { get; set; }
        public bool ActiveInd { get; set; } = true;
    }
}
