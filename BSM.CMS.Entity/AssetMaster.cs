﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class AssetMaster: BaseEntity
    {
        [Key]
        public int AssetmasterId { get; set; }
        public string AssetName { get; set; }
        public string Description { get; set; }
        public int AssetType { get; set; }
        public string Assetpath { get; set; }
        public int status { get; set; }
        public int CompId { get; set; }


        [ForeignKey("Section")]
        public int SectionMasterId { get; set; }
        public SectionMaster Section { get; set; }

        public ICollection<AssetMeta> assetMetas { get; set; }
    }
}
