﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class SectionMaster:BaseEntity
    {
        [Key]
        public int SectionMasterId { get; set; }
        [StringLength(50)]
        public string SectionCode { get; set; }
        [StringLength(500)]
        public string SectionName { get; set; }
        public int CompId { get; set; }

        public ICollection<AssetMaster> assetMasters { get; set; }  
    }
}
