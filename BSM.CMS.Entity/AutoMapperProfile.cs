﻿using AutoMapper;
using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Entity
{
    public class AutoMapperProfile :Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AssetViewModel, AssetMaster>(MemberList.None);
            CreateMap<AssetMaster, AssetViewModel>(MemberList.None);

             CreateMap<SectionMaster, SectionViewModel>(MemberList.None);
            CreateMap<SectionViewModel, SectionMaster>(MemberList.None);
               // .ForMember(x => x.assetMasters, opt => opt.Ignore());

            // CreateMap<AssetMeta, AssetMetaViewModel>(MemberList.None);
            //CreateMap<AssetMaster, AssetViewModel>(MemberList.None);

            CreateMap<AssetCollectionMaster, CollectionViewModel>(MemberList.None);
            CreateMap<CollectionViewModel, AssetCollectionMaster>(MemberList.None);
            
            CreateMap<AssetCollectionDetails, CollectionDetailViewModel>(MemberList.None);
            CreateMap<CollectionDetailViewModel, AssetCollectionDetails>(MemberList.None);
            
            CreateMap<RegisterViewModel, User>(MemberList.None);
            //CreateMap<CollectionDetailViewModel, AssetCollectionDetails>(MemberList.None);

            CreateMap<AssetMeta, AssetMetaViewModel>(MemberList.None);
            CreateMap<AssetMetaViewModel, AssetMeta>(MemberList.None);


        }
    }
}
