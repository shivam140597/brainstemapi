﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSM.CMS.Entity;
using BSM.CMS.Interface;
using BSM.CMS.DAL;

namespace BSM.CMS.Repository
{
    public class EmployeeRepository : IEmployee
    {
        private readonly CMSContext cmsContext;
        public EmployeeRepository(CMSContext _cmsContext)
        {
            this.cmsContext = _cmsContext;
        }
        public List<User> GetEmployees()
        {
            List<User> employees = (from emp in cmsContext.Users
                                        select emp).ToList();
            return employees;
        }
    }
}
