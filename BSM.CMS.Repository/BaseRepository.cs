﻿using AutoMapper;
using BSM.CMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class BaseRepository
    {
       protected readonly CMSContext _context;
        protected readonly IMapper _mapper;
        public BaseRepository(CMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;   
        }
       }
}
