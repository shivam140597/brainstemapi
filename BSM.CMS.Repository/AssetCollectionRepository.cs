﻿using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class AssetCollectionRepository:BaseRepository,IAssetCollectionMaster
    {
        public AssetCollectionRepository(CMSContext context,IMapper mapper) : base(context,mapper) { }

        public async Task<CollectionViewModel> GetCollectionById(int id)
        {
            CollectionViewModel model = new CollectionViewModel();
            var result = await _context.AssetCollectionMasters.FirstOrDefaultAsync(u => u.AssetCollectionMasterId == id);
            if (result == null)
                return model;
            else
            {
                

                //mapping of assetcollectionMaster to AssetCollectionViewModel
                model=_mapper.Map<CollectionViewModel>(result); 
                return model;
            }

        }
        public async Task<int?> AddCollection(CollectionViewModel model)
        {
            AssetCollectionMaster collection = new AssetCollectionMaster();
            //map the assetcollectionMaster and AssetCollectionViewModel
            collection = _mapper.Map<AssetCollectionMaster>(model);
            await _context.AssetCollectionMasters.AddAsync(collection);
            var result = await _context.SaveChangesAsync();

            return result;
        }
        public async Task<List<CollectionViewModel>> GetAllCollection()
        {
            List<CollectionViewModel> collections = new List<CollectionViewModel>();
            var result = await _context.AssetCollectionMasters.ToListAsync();
            if (result == null)
                return collections;
            else
            {

                //mapping of assetcollectionMaster to AssetCollectionViewModel
                collections = _mapper.Map<List<CollectionViewModel>>(result);
                return collections;
            }

        }

    }
}
