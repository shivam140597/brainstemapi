﻿using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class AssetMetaRepository : BaseRepository , IAssetMeta
    {
        public AssetMetaRepository(CMSContext context, IMapper mapper) : base(context, mapper ) { }
        public async Task<AssetMeta> GetAssetMetaById(int id)
        {
            AssetMeta model = new AssetMeta();
            var result = await _context.AssetMetas.FirstOrDefaultAsync(u => u.AssetMetaId == id);
            if (result == null)
                return model;
            else
            {
                
                 //model= _mapper.Map<AssetMeta>(result);   
                //mapping of AssetMeta to assetMetaViewModel if assetMetaViewModel Exist other wise return result

                return result;
            }

        }

        public async Task<List<AssetMeta>> GetMultipleAssetMeta(int AssetMetaId)
        {
            List<AssetMeta> model = new List<AssetMeta>();
            var result = await _context.AssetMetas.Where(u => u.AssetMetaId == AssetMetaId).ToListAsync();
            if (result == null)
                return model;
            else
            {
                
                //mapping of ListAssetMeta to listassetMetaviewModel if assetMetaViewModel Exist other wise return result
                return result;
            }
        }

        // This can be use for the getting asset via collection.
        //public async Task<List<AssetViewModel>> GetMultipleAssetMeta(List<int> assetIds)
        //{
        //    List<AssetViewModel> Assets = new List<AssetViewModel>();
        //    foreach (var asset in assetIds)
        //    {
        //        Assets.Add(await GetAssetById(asset));
        //    }
        //    return Assets;
        //}

        public async Task<int?> AddAssetMeta(AssetMetaViewModel model)
        {
            AssetMeta assetmeta = new AssetMeta();
            //map the assetmaster and AssetViewModel
            assetmeta=_mapper.Map<AssetMeta>(model);
            await _context.AssetMetas.AddAsync(assetmeta);
            var result = await _context.SaveChangesAsync();

            return result;
        }
    }
}
