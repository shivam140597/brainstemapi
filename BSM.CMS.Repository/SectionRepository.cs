﻿using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class SectionRepository:BaseRepository, ISection
    {
        public SectionRepository(CMSContext context, IMapper mapper) : base(context, mapper ) { }
       
        public async Task<SectionViewModel> GetSectionById(int id)
        {
            SectionViewModel model = new SectionViewModel();
            var result = await _context.SectionMasters.FirstOrDefaultAsync(u => u.SectionMasterId == id);
            if (result == null)
                return model;
            else
            {
                model= _mapper.Map<SectionViewModel>(result);
                //mapping of SectionMaster to SectionViewModel
                return model;
            }

        }

        public async Task<List<SectionViewModel>> GetAllSection()
        {
            List<SectionViewModel> Sections = new List<SectionViewModel>();
            var result = await _context.SectionMasters.ToListAsync();
            // mapping
            if(result == null)
            {
                return Sections;
            }
            Sections= _mapper.Map<List<SectionViewModel>>(result);
            return Sections;
        }


        public async Task<List<SectionViewModel>> GetMultipleSection(List<int> SectionIds)
        {
            List<SectionViewModel> sections = new List<SectionViewModel>();
            foreach (var section in SectionIds)
            {
                sections.Add(await GetSectionById(section));
            }
            return sections;
        }

        public async Task<int?> AddSection(SectionViewModel model)
        {
            try
            {
                SectionMaster section = new SectionMaster();
                //map the assetmaster and AssetViewModel
                //_mapper.Map(section, model);
                section = _mapper.Map<SectionMaster>(model);
                await _context.SectionMasters.AddAsync(section);
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            

            
        }
    }
}
