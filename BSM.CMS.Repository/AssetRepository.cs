﻿
using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class AssetRepository:BaseRepository,IAsset
    {
        public AssetRepository(CMSContext context,IMapper  mapper ) : base(context, mapper) { }
       
        //CRUD OPERATIONS
        public async Task<AssetViewModel> GetAssetById(int id)
        {
            try
            {
                AssetViewModel model = new AssetViewModel();
                var result = await _context.AssetMasters.FirstOrDefaultAsync(u => u.AssetmasterId == id);
                if (result == null)
                    return model;
                else
                {
                    
                     model = _mapper.Map<AssetViewModel>(result);
                    
                    return model;
                }
            }
            catch (Exception)
            {

                throw ;
            }
            
            
        }

        public async Task<List<AssetViewModel>> GetAssetBySection(int sectionmasterId)
        {
            List<AssetViewModel> model = new List<AssetViewModel>();
            var result = await _context.AssetMasters.Where(u => u.SectionMasterId == sectionmasterId).ToListAsync();
            if (result == null)
                return model;
            else
            {
               
                model = _mapper.Map<List<AssetViewModel>>(result);
                return model;
            } 
        }

        // This can be use for the getting asset via collection.
        public async Task<List<AssetViewModel>> GetMultipleAsset(List<int> assetIds)
        {
            List<AssetViewModel> Assets = new List<AssetViewModel>();   
            foreach(var asset in assetIds)
            {
                Assets.Add(await GetAssetById(asset));
            }
            return Assets;
        }

        public async Task<List<AssetViewModel>> GetAllAsset()
        {
            List<AssetViewModel> Assets = new List<AssetViewModel>();
            var result = await _context.AssetMasters.ToListAsync();
            // mapping
            Assets = _mapper.Map<List<AssetViewModel>>(result);
            return Assets;
        }


        public async Task<int?> AddAsset(AssetViewModel model)
        {
            try
            {
                AssetMaster asset = new AssetMaster();
                //map the assetmaster and AssetViewModel
                asset=_mapper.Map<AssetMaster>(model);
                await _context.AssetMasters.AddAsync(asset);
                var result = await _context.SaveChangesAsync();

                return result;
            }
            catch (Exception)
            {

                throw;
            }
           
        }

    }
}
