﻿using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class AssetCollectionDetailRepository :BaseRepository,IAssetCollectionDetails
    {
        public AssetCollectionDetailRepository(CMSContext context , IMapper mapper): base(context,mapper) { }
       
        public async Task<CollectionDetailViewModel> GetCollectionDetailsById(int id)
        {
            CollectionDetailViewModel model = new CollectionDetailViewModel();
            var result = await _context.AssetCollectionDetails.FirstOrDefaultAsync(u => u.AssetCollectionDetailsId == id);
            if (result == null)
                return model;
            else
            {
                
                //mapping of AssetCollectionMasters to CollectionDetailViewModel
                model=_mapper.Map<CollectionDetailViewModel>(result);
                return model;
            }

        }
        public async Task<int?> AddCollectionDetails(CollectionDetailViewModel model)
        {
            AssetCollectionDetails collectionDetails = new AssetCollectionDetails();
            //map the AssetCollectionMasters and CollectionDetailViewModel
            collectionDetails = _mapper.Map<AssetCollectionDetails>(model);
            await _context.AssetCollectionDetails.AddAsync(collectionDetails);
            var result = await _context.SaveChangesAsync();

            return result;
        }
        public async Task<List<CollectionViewModel>> GetAllCollectionDetails()
        {
            List<CollectionViewModel> collections = new List<CollectionViewModel>();
            var result = await _context.AssetCollectionMasters.ToListAsync();
            if (result == null)
                return collections;
            else
            {
                
                //mapping of assetcollectionMaster to AssetCollectionViewModel
                collections=_mapper.Map<List<CollectionViewModel>>(result);
                return collections;
            }

        }
    }
}
