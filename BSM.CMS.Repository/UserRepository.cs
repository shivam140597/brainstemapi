﻿using AutoMapper;
using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Repository
{
    public class UserRepository : BaseRepository,IUser
    {
        #region Global
        
        public UserRepository(CMSContext context, IMapper mapper) : base(context, mapper )
        {
            
        }
        #endregion

        // Login Service 
        public async Task<int> LogIn(LoginViewModel model)
        {
            var user = await _context.Users.AsNoTracking().FirstOrDefaultAsync(u => (u.UserEmail == model.UserEmail) && u.Password == model.Password);
            if (user == null)
                return 0;
            else
                return 1;
        }

        public async Task<int> Registration(RegisterViewModel model)
        {
            User NewUser = new User();

            // automap the RegisterViewModel & User
            //NewUser.UserEmail = model.UserEmail;
            //NewUser.Password = model.Password;
            NewUser= _mapper.Map<User>(model);
            await _context.Users.AddAsync(NewUser);
            var result = await _context.SaveChangesAsync();
            return result;
        }
    }
}
