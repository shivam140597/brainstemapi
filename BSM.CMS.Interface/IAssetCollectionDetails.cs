﻿using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface IAssetCollectionDetails
    {
        Task<CollectionDetailViewModel> GetCollectionDetailsById(int id);
        Task<int?> AddCollectionDetails(CollectionDetailViewModel model);
    }
}
