﻿using System;
using System.Collections.Generic;
using BSM.CMS.Entity;

namespace BSM.CMS.Interface
{
    public interface IEmployee
    {
        List<User> GetEmployees();
    }
}
