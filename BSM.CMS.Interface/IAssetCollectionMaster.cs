﻿using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface IAssetCollectionMaster
    {
        Task<CollectionViewModel> GetCollectionById(int id);
        Task<int?> AddCollection(CollectionViewModel model);

        Task<List<CollectionViewModel>> GetAllCollection();

    }
}
