﻿using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface IAsset
    {
        Task<AssetViewModel> GetAssetById(int id);
        Task<List<AssetViewModel>> GetAssetBySection(int sectionId);
        Task<List<AssetViewModel>> GetMultipleAsset(List<int> assetIds);
        Task<List<AssetViewModel>> GetAllAsset();
        Task<int?> AddAsset(AssetViewModel model);
    }
}
