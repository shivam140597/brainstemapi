﻿using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface IUser
    {
          Task<int> LogIn(LoginViewModel model);
        Task<int> Registration(RegisterViewModel model);
    }
}
