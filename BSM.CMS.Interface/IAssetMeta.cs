﻿using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface IAssetMeta
    {
        Task<AssetMeta> GetAssetMetaById(int id);
        
        //Task<List<AssetViewModel>> GetAssetMetaBy(int sectionId);
        Task<List<AssetMeta>> GetMultipleAssetMeta(int assetMasterId);
        Task<int?> AddAssetMeta(AssetMetaViewModel model);
    }
}
