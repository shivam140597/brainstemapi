﻿using BSM.CMS.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Interface
{
    public interface ISection
    {
        Task<SectionViewModel> GetSectionById(int id);
        Task<List<SectionViewModel>> GetAllSection();
        Task<List<SectionViewModel>> GetMultipleSection(List<int> SectionIds);
        Task<int?> AddSection(SectionViewModel model);
    }
}
