using Microsoft.AspNetCore.Mvc;
using BSM.CMS.Interface;
using BSM.CMS.Entity;

namespace BSM.CMS.API.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private IEmployee employeeRepository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IEmployee _employee)
        {
            _logger = logger;
            employeeRepository = _employee;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
        [HttpGet(Name ="GetAllEmployees")]
        public IActionResult GetEmployees()
        {
            return Ok(employeeRepository.GetEmployees());
        }
    }
}