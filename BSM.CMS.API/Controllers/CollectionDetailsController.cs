﻿using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionDetailsController : ControllerBase
    {
        private readonly IAssetCollectionDetails _collectionDetails;
        public CollectionDetailsController(IAssetCollectionDetails collectionDetails)
        {
            _collectionDetails = collectionDetails;
        }

        #region GetCollectionDetailById
        [HttpGet(nameof(GetCollectionDetailsById))]
        public async Task<IActionResult> GetCollectionDetailsById(int id)
        {
            try
            {
                var result = await _collectionDetails.GetCollectionDetailsById(id);
                if (result.AssetCollectionDetailsId == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion



        #region Post Collection Details
        [HttpPost(nameof(PostCollectionDetails))]
        public async Task<IActionResult> PostCollectionDetails([FromBody]CollectionDetailViewModel model)
        {
            try
            {
                var result = await _collectionDetails.AddCollectionDetails(model);
                if (result == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.UnprocessableEntity,
                        StatusCode = APIResponseCode.UnprocessableEntity.GetResponseCode()
                    });
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.Success,
                        StatusCode = APIResponseCode.OK.GetResponseCode()
                    });
                }
            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion
    }
}
