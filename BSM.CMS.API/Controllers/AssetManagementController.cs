﻿using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetManagementController : ControllerBase
    {

        #region Global DI
        private readonly IAsset _assetManagement;
        public AssetManagementController(IAsset assetMangement)
        {
            _assetManagement = assetMangement;  
        }
        #endregion

        #region GetById
        [HttpGet(nameof(GetAssetById))]
        public async Task<IActionResult> GetAssetById(int id)
        {
            try
            {
                var result = await _assetManagement.GetAssetById(id);
                if(result.AssetmasterId ==0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode =APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    result.ResponseMessage = ResponseMessages.Success;
                    result.StatusCode =APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }
                    
            }
            catch 
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        #region GetAllAsset
        [HttpGet(nameof(GetAllAsset))]
        public async Task<IActionResult> GetAllAsset()
        {
            try
            {
                var result = await _assetManagement.GetAllAsset();
                if (result.Count == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                   // result.ResponseMessage = ResponseMessages.Success;
                   // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        #region Post Asset
        [HttpPost(nameof(PostAsset))]
        public async Task<IActionResult> PostAsset([FromBody]AssetViewModel model)
        {
            try
            {
                var result= await _assetManagement.AddAsset(model);  
                if(result==0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.UnprocessableEntity,
                        StatusCode = APIResponseCode.UnprocessableEntity.GetResponseCode()
                    }) ;
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.Success,
                        StatusCode = APIResponseCode.OK.GetResponseCode()
                    });
                }
            }
            catch 
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

    }
}
