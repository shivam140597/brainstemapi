﻿using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectionManagementController : ControllerBase
    {
        private readonly ISection _setionManagement;
        public SectionManagementController(ISection sectionManagement)
        {
            _setionManagement = sectionManagement;
        }

        #region GetSectionById
        [HttpGet(nameof(GetSectionById))]
        public async Task<IActionResult> GetSectionById(int id)
        {
            try
            {
                var result = await _setionManagement.GetSectionById(id);
                if (result.SectionMasterId == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        #region GetAllSection
        [HttpGet(nameof(GetAllSection))]
        public async Task<IActionResult> GetAllSection()
        {
            try
            {
                var result = await _setionManagement.GetAllSection();
                if (result.Count == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion  

        #region Post Asset
        [HttpPost(nameof(PostSection))]
        public async Task<IActionResult> PostSection([FromBody]SectionViewModel model)
        {
            try
            {

                var result = await _setionManagement.AddSection(model);
                if (result == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.UnprocessableEntity,
                        StatusCode = APIResponseCode.UnprocessableEntity.GetResponseCode()
                    });
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.Success,
                        StatusCode = APIResponseCode.OK.GetResponseCode()
                    });
                }
            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion
    }

}
