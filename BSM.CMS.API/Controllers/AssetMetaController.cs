﻿using BSM.CMS.Entity;
using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetMetaController : ControllerBase
    {
        private readonly IAssetMeta _assetMeta;
        public AssetMetaController(IAssetMeta assetMeta)
        {
            _assetMeta = assetMeta;
        }

        #region GetassetMetaById
        [HttpGet(nameof(GetAssetMetaById))]
        public async Task<IActionResult> GetAssetMetaById(int id)
        {
            try
            {
                var result = await _assetMeta.GetAssetMetaById(id);
                if (result.AssetMetaId == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        

        #region Post AssetMeta
        [HttpPost(nameof(PostAssetMeta))]
        public async Task<IActionResult> PostAssetMeta([FromBody]AssetMetaViewModel model)
        {
            try
            {
                var result = await _assetMeta.AddAssetMeta(model);
                if (result == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.UnprocessableEntity,
                        StatusCode = APIResponseCode.UnprocessableEntity.GetResponseCode()
                    });
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.Success,
                        StatusCode = APIResponseCode.OK.GetResponseCode()
                    });
                }
            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion
    }
}
