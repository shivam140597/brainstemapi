﻿using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        #region Global
        private readonly IUser _iUser;

        public AccountController(IUser iUser)
        {
            _iUser = iUser;
        }
        #endregion

        #region LogIn
        [HttpPost(nameof(login))]
        public async Task<IActionResult> login([FromBody] LoginViewModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var response = await this._iUser.LogIn(model);
                    if (response == 0)
                    {
                        return Ok(new BaseViewModel
                        {
                            ResponseMessage = ResponseMessages.NoRecordFound,
                            StatusCode = APIResponseCode.NotFound.GetResponseCode()
                        });
                    }
                    else
                    {
                        return Ok(new BaseViewModel
                        {
                            ResponseMessage = ResponseMessages.Success,
                            StatusCode = APIResponseCode.OK.GetResponseCode()
                        });
                    }
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ModelState.Values.Select(x => x.Errors).First().First().ErrorMessage,
                        StatusCode = APIResponseCode.BadRequest.GetResponseCode()
                    });
                }
            }
            catch
            {
                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        #region UserRegistration
        [HttpPost(nameof(Register))]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await this._iUser.Registration(model);
                    if (response == 1)
                    {
                        
                        return Ok(new BaseViewModel
                        {
                            ResponseMessage = ResponseMessages.Success,
                            StatusCode = APIResponseCode.OK.GetResponseCode()
                        });
                    }
                    else
                    {
                        return Ok(new BaseViewModel
                        {
                            ResponseMessage = ResponseMessages.ServerError,
                            StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                        });
                    }
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ModelState.Values.Select(x => x.Errors).First().First().ErrorMessage,
                        StatusCode = APIResponseCode.BadRequest.GetResponseCode()
                    });
                }
            }
            catch
            {
                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

    }
}
