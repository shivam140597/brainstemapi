﻿using BSM.CMS.Entity.ViewModel;
using BSM.CMS.Interface;
using BSM.CMS.Utility.Enums;
using BSM.CMS.Utility.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BSM.CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetCollectionMangementController : ControllerBase
    {
        private readonly IAssetCollectionMaster _assetCollection;
        public AssetCollectionMangementController(IAssetCollectionMaster assetCollection)
        {
            _assetCollection = assetCollection;
        }

        #region GetCollectionById
        [HttpGet(nameof(GetCollectionById))]
        public async Task<IActionResult> GetCollectionById(int id)
        {
            try
            {
                var result = await _assetCollection.GetCollectionById(id);
                if (result.AssetCollectionMasterId == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion

        #region GetAllCollection
        [HttpGet(nameof(GetAllCollection))]
        public async Task<IActionResult> GetAllCollection()
        {
            try
            {
                var result = await _assetCollection.GetAllCollection();
                if (result.Count == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.NoRecordFound,
                        StatusCode = APIResponseCode.NotFound.GetResponseCode()
                    });
                }
                else
                {
                    // result.ResponseMessage = ResponseMessages.Success;
                    // result.StatusCode = APIResponseCode.OK.GetResponseCode();
                    return Ok(result);
                }

            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion  

        #region Post Collection
        [HttpPost(nameof(PostCollection))]
        public async Task<IActionResult> PostCollection([FromBody]CollectionViewModel model)
        {
            try
            {
                var result = await _assetCollection.AddCollection(model);
                if (result == 0)
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.UnprocessableEntity,
                        StatusCode = APIResponseCode.UnprocessableEntity.GetResponseCode()
                    });
                }
                else
                {
                    return Ok(new BaseViewModel
                    {
                        ResponseMessage = ResponseMessages.Success,
                        StatusCode = APIResponseCode.OK.GetResponseCode()
                    });
                }
            }
            catch
            {

                return Ok(new BaseViewModel
                {
                    ResponseMessage = ResponseMessages.ServerError,
                    StatusCode = APIResponseCode.InternalServerError.GetResponseCode()
                });
            }
        }
        #endregion
    }
}
