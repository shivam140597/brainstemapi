using BSM.CMS.DAL;
using BSM.CMS.Entity;
using BSM.CMS.Interface;
using BSM.CMS.Repository;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

ConfigurationManager Configuration = builder.Configuration;
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(AutoMapperProfile));

//Register services
//builder.Services.AddDbContext<CMSContext>(options => options.UseMySQL(
//  Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddDbContext<CMSContext>(options => options.UseSqlServer(
    Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddScoped<IAsset, AssetRepository>();
builder.Services.AddScoped<ISection, SectionRepository>();
builder.Services.AddScoped<IAssetMeta, AssetMetaRepository>();
builder.Services.AddScoped<IAssetCollectionMaster, AssetCollectionRepository>();
builder.Services.AddScoped<IAssetCollectionDetails, AssetCollectionDetailRepository>();
builder.Services.AddScoped<IUser, UserRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

