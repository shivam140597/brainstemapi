﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BSM.CMS.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssetCollectionMasters",
                columns: table => new
                {
                    AssetCollectionMasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CollectionName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CompId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ActiveInd = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetCollectionMasters", x => x.AssetCollectionMasterId);
                });

            migrationBuilder.CreateTable(
                name: "SectionMasters",
                columns: table => new
                {
                    SectionMasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SectionCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    SectionName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    CompId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ActiveInd = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SectionMasters", x => x.SectionMasterId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserEmail = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhoneNo = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationsAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Pincode = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ActiveInd = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "AssetMasters",
                columns: table => new
                {
                    AssetmasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssetName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AssetType = table.Column<int>(type: "int", nullable: false),
                    Assetpath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    status = table.Column<int>(type: "int", nullable: false),
                    CompId = table.Column<int>(type: "int", nullable: false),
                    SectionMasterId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedDttm = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ActiveInd = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetMasters", x => x.AssetmasterId);
                    table.ForeignKey(
                        name: "FK_AssetMasters_SectionMasters_SectionMasterId",
                        column: x => x.SectionMasterId,
                        principalTable: "SectionMasters",
                        principalColumn: "SectionMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssetCollectionDetails",
                columns: table => new
                {
                    AssetCollectionDetailsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActiveInd = table.Column<bool>(type: "bit", nullable: false),
                    AssetCollectionMasterid = table.Column<int>(type: "int", nullable: false),
                    AssetmasterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetCollectionDetails", x => x.AssetCollectionDetailsId);
                    table.ForeignKey(
                        name: "FK_AssetCollectionDetails_AssetCollectionMasters_AssetCollectionMasterid",
                        column: x => x.AssetCollectionMasterid,
                        principalTable: "AssetCollectionMasters",
                        principalColumn: "AssetCollectionMasterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetCollectionDetails_AssetMasters_AssetmasterId",
                        column: x => x.AssetmasterId,
                        principalTable: "AssetMasters",
                        principalColumn: "AssetmasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssetMetas",
                columns: table => new
                {
                    AssetMetaId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MetaKey = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MetaValue = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ActiveInd = table.Column<bool>(type: "bit", nullable: false),
                    AssteMasterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetMetas", x => x.AssetMetaId);
                    table.ForeignKey(
                        name: "FK_AssetMetas_AssetMasters_AssteMasterId",
                        column: x => x.AssteMasterId,
                        principalTable: "AssetMasters",
                        principalColumn: "AssetmasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssetCollectionDetails_AssetCollectionMasterid",
                table: "AssetCollectionDetails",
                column: "AssetCollectionMasterid");

            migrationBuilder.CreateIndex(
                name: "IX_AssetCollectionDetails_AssetmasterId",
                table: "AssetCollectionDetails",
                column: "AssetmasterId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetMasters_SectionMasterId",
                table: "AssetMasters",
                column: "SectionMasterId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetMetas_AssteMasterId",
                table: "AssetMetas",
                column: "AssteMasterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetCollectionDetails");

            migrationBuilder.DropTable(
                name: "AssetMetas");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "AssetCollectionMasters");

            migrationBuilder.DropTable(
                name: "AssetMasters");

            migrationBuilder.DropTable(
                name: "SectionMasters");
        }
    }
}
