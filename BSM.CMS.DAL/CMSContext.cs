﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BSM.CMS.Entity;

namespace BSM.CMS.DAL
{
    public class CMSContext : DbContext
    {
        public CMSContext(DbContextOptions<CMSContext> options) : base(options) { }
        public virtual DbSet<SectionMaster> SectionMasters { get; set; }
        public virtual DbSet<AssetMaster> AssetMasters { get; set; }
        public virtual DbSet<AssetMeta> AssetMetas { get; set; }
        public virtual DbSet<AssetCollectionMaster> AssetCollectionMasters { get; set; }
        public virtual DbSet<AssetCollectionDetails> AssetCollectionDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }

    }
}
