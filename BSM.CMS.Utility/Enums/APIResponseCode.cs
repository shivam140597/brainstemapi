﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Utility.Enums
{
    public enum APIResponseCode
    {
        OK = 200,
        BadRequest = 400,
        NotFound = 404,
        UnprocessableEntity = 422,
        InternalServerError = 500,
        AllReadyExist = 409,
    }
}
