﻿using BSM.CMS.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSM.CMS.Utility.Helpers
{
    
    public static class Converters
    {
        public static int GetResponseCode(this APIResponseCode resCode)
        {
            return (int)resCode;
        }
    }
}
